package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"reflect"
	"runtime"
	"strings"
	"time"

	"gitlab.com/mergetb/tech/shared/storage/decompression"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"gitlab.com/mergetb/api/facility/v1/go"
	"google.golang.org/grpc"
)

var (
	benchmark_runs = 2
)

// read <name>=<value> from kernel parameters
func readKernelParam(name string) (string, error) {

	cmdline, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return "", fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}

	args := strings.Fields(string(cmdline))
	var value string
	for _, x := range args {
		parts := strings.Split(x, "=")
		if len(parts) == 2 {
			if parts[0] == name {
				value = parts[1]
			}
		}
	}

	if value == "" {
		return "", fmt.Errorf("%s kernel parameter not found", name)
	}

	return value, nil

}

func sledClient() (facility.SledClient, *grpc.ClientConn, error) {

	log.Printf("connecting to sled")

	conn, err := grpc.Dial("sled:6004", grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("dial sled: %v", err)
	}

	client := facility.NewSledClient(conn)

	return client, conn, err

}

func marsMinIOImageClient(server string) (*minio.Client, error) {

	return minio.New(
		fmt.Sprintf("%s", server),
		&minio.Options{
			Creds: credentials.NewStaticV4(
				"image",
				"imageread",
				"",
			),
			//TODO Secure: true, // implies ssl
		})

}

func benchImages(dest, server string) error {

	log.Printf("Starting image benchmark")

	boots := []string{
		"bios",
		"efi",
	}

	rootfses := []string{
		"tc12-rootfs",
		"tc13-rootfs",
		"bullseye-rootfs",
		"buster-rootfs",
		"1804-rootfs",
		"2004-rootfs",
		"hypervisor-rootfs",
	}

	exts := []struct {
		Ext     string
		Decompr decompression.Decompressor
	}{
		{".gz", decompression.DecompressGZIP},
		{".xz", decompression.DecompressXZ},
		{".raw", decompression.DecompressRAW},
		{".zst3", decompression.DecompressZSTDGolang},
		{".zst22", decompression.DecompressZSTDGolang},
		{".zst3", decompression.DecompressZSTDBinary},
		{".zst22", decompression.DecompressZSTDBinary},
		{".lz4", decompression.DecompressLZ4},
	}

	dests := []string{
		"/dev/null",
		dest,
	}

	for _, mapping := range exts {
		for _, boot := range boots {
			for _, rootfs := range rootfses {
				for _, loc := range dests {
					ext := mapping.Ext
					decompr := mapping.Decompr

					image := fmt.Sprintf("%s/%s%s", boot, rootfs, ext)

					err := benchImage(image, loc, server, decompr)
					if err != nil {
						log.Printf("benchImage: %v", err)
					}
				}
			}
		}
	}

	log.Printf("Image benchmark complete!")

	return nil

}

func benchImage(name, dest, server string, decompr decompression.Decompressor) error {

	var total time.Duration

	for runs := 0; runs < benchmark_runs; runs++ {

		start := time.Now()
		err := stampImageDecompressor(name, dest, server, &decompr)
		taken := time.Since(start)

		if err != nil {
			log.Printf("stamp image err: %v", err)
			time.Sleep(5 * time.Second)
		} else {
			total += taken
		}

	}

	fname := runtime.FuncForPC(reflect.ValueOf(decompr).Pointer()).Name()
	average := total / time.Duration(benchmark_runs)

	log.Printf("\n\nImage: %s\nFunction: %s\nDest: %s\n  Runs: %d\n  Total: %s\n  Average: %s\n\n", name, fname, dest, benchmark_runs, total.String(), average.String())

	return nil

}
