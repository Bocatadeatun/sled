#!/bin/bash

# this is an incredibly janky workaround to allow u-root to compile with v1.14
# the problem is that it uses an os check that only exists in v1.15+, so we patch it out

file="vendor/golang.org/x/net/http2/transport.go"

sed -i 's/if n < len(p) && nn > 0 && errors.Is(err, os.ErrDeadlineExceeded)/if false/g' $file
sed -i 's|"os"|//"os"|g' $file
